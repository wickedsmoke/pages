var bg_color_img_box = 'rgba(0,0,0,0.9)'
var allow_hide_scroll_img_box = 'yes'
var use_fade_inout_img_box = 'yes'
var speed_img_box = 0.08
var z_index_dv_img_box = 999
var vopa_img_box, img_popup

window.onload = function() {
	var crtdv_img_box = document.createElement('div')
	crtdv_img_box.id = 'img_box'
	document.getElementsByTagName('body')[0].appendChild(crtdv_img_box)
	img_popup = document.getElementById("img_box")
	img_popup.style.top = 0
	img_popup.style.left = 0
	img_popup.style.opacity = 0
	img_popup.style.width = '100%'
	img_popup.style.height = '100%'
	img_popup.style.display = 'none'
	img_popup.style.position = 'fixed'
	img_popup.style.cursor = 'pointer'
	img_popup.style.textAlign = 'center'
	img_popup.style.zIndex = z_index_dv_img_box
	img_popup.style.backgroundColor = bg_color_img_box
}

function img_box(self) {
	var namepic_img_box = typeof self === 'string' ? self : self.src
	vopa_img_box = 0
	var winH = window.innerHeight
	var winW = window.innerWidth
	var imgH, padtop, idfadein_img_box
	var img = new Image()
	img.src = namepic_img_box
	img.onload = function() {
		imgH = img.height
		img_popup.innerHTML = '<img src=' + namepic_img_box + '>'

		if (img.width > winW) {
			img_popup.getElementsByTagName('img')[0].style.width = '90%'
			imgH = img.height * (winW * 90 / 100) / img.width
		} else if (imgH > winH) {
			img_popup.getElementsByTagName('img')[0].style.height = '90%'
			imgH = winH * 90 / 100
		}

		padtop = (imgH < winH) ? (winH - imgH) / 2 : 0
		img_popup.style.paddingTop = padtop + 'px'

		if (allow_hide_scroll_img_box == 'yes')
			document.body.style.overflow = 'hidden'

		img_popup.style.display = 'block'
	}

	if (use_fade_inout_img_box == 'yes') {
		idfadein_img_box = setInterval(function() {
			if (vopa_img_box <= 1.1) {
				img_popup.style.opacity = vopa_img_box
				vopa_img_box += speed_img_box
			} else {
				img_popup.style.opacity = 1
				clearInterval(idfadein_img_box)
			}
		}, 10)
	} else {
		img_popup.style.opacity = 1
	}

	img_popup.onclick = function() {
		if (use_fade_inout_img_box == 'yes') {
			var idfadeout_img_box = setInterval(function() {
				if (vopa_img_box >= 0) {
					img_popup.style.opacity = vopa_img_box
					vopa_img_box -= speed_img_box
				} else {
					img_popup.style.opacity = 0
					clearInterval(idfadeout_img_box)
					img_popup.style.display = 'none'
					img_popup.innerHTML = ''
					document.body.style.overflow = 'visible'
					vopa_img_box = 0
				}
			}, 10)
		} else {
			img_popup.style.opacity = 0
			img_popup.style.display = 'none'
			img_popup.innerHTML = ''
			document.body.style.overflow = 'visible'
		}
	}
}
